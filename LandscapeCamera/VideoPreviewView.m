//
//  VideoPreviewView.m
//  LandscapeCamera
//
//  Created by Jon Drukman on 4/5/20.
//  Copyright © 2020 Jon Drukman. All rights reserved.
//

#import "VideoPreviewView.h"
@import AVFoundation;

@implementation VideoPreviewView

+ (Class)layerClass
{
    return [AVCaptureVideoPreviewLayer class];
}

- (AVCaptureVideoPreviewLayer*) videoPreviewLayer
{
    return (AVCaptureVideoPreviewLayer *)self.layer;
}

- (AVCaptureSession*) session
{
    return self.videoPreviewLayer.session;
}

- (void)setSession:(AVCaptureSession*) session
{
    self.videoPreviewLayer.session = session;
}

-(void)layoutSubviews {
    self.backgroundColor = [UIColor blackColor];
}

@end
