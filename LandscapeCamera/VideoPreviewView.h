//
//  VideoPreviewView.h
//  LandscapeCamera
//
//  Created by Jon Drukman on 4/5/20.
//  Copyright © 2020 Jon Drukman. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AVCaptureVideoPreviewLayer;
@class AVCaptureSession;

NS_ASSUME_NONNULL_BEGIN

@interface VideoPreviewView : UIView
@property (nonatomic, readonly) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic) AVCaptureSession *session;
@end

NS_ASSUME_NONNULL_END
