//
//  ViewController.m
//  LandscapeCamera
//
//  Created by Jon Drukman on 4/3/20.
//  Copyright © 2020 Jon Drukman. All rights reserved.
//

@import AVFoundation;

#import "ViewController.h"
#import "VideoPreviewView.h"

@interface ViewController ()
@property (nonatomic) AVCaptureSession* session;
@property (weak, nonatomic) IBOutlet VideoPreviewView *previewView;
@property (nonatomic) AVCaptureDeviceInput* videoDeviceInput;
@property (nonatomic) dispatch_queue_t sessionQueue;
@property (nonatomic, strong) AVCaptureDeviceType currentlySelectedCamera;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.session = [[AVCaptureSession alloc] init];
    
    // Set up the preview view.
    self.previewView.session = self.session;
    self.sessionQueue = dispatch_queue_create("session queue", DISPATCH_QUEUE_SERIAL);
    dispatch_async(self.sessionQueue, ^{
        [self configureSession];
        [self.session startRunning];
    });
}

-(void)configureSession {
    [self.session beginConfiguration];
    
    self.session.sessionPreset = AVCaptureSessionPreset1920x1080;
    
    AVCaptureDevice* videoDevice = [AVCaptureDevice defaultDeviceWithDeviceType:AVCaptureDeviceTypeBuiltInUltraWideCamera
                                                                      mediaType:AVMediaTypeVideo
                                                                       position:AVCaptureDevicePositionBack];
    self.currentlySelectedCamera = AVCaptureDeviceTypeBuiltInUltraWideCamera;

    if (!videoDevice) {
        videoDevice = [AVCaptureDevice defaultDeviceWithDeviceType:AVCaptureDeviceTypeBuiltInWideAngleCamera
                                                         mediaType:AVMediaTypeVideo
                                                          position:AVCaptureDevicePositionBack];
    }
    NSError *error;
    AVCaptureDeviceInput* videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error];
    if (!videoDeviceInput) {
        NSLog(@"Could not create video device input: %@", error);
        [self.session commitConfiguration];
        return;
    }
    if ([self.session canAddInput:videoDeviceInput]) {
        [self.session addInput:videoDeviceInput];
        self.videoDeviceInput = videoDeviceInput;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            /*
             Dispatch video streaming to the main queue because AVCaptureVideoPreviewLayer is the backing layer for PreviewView.
             You can manipulate UIView only on the main thread.
             Note: As an exception to the above rule, it is not necessary to serialize video orientation changes
             on the AVCaptureVideoPreviewLayer’s connection with other session manipulation.
             
             Use the status bar orientation as the initial video orientation. Subsequent orientation changes are
             handled by CameraViewController.viewWillTransition(to:with:).
            */
            AVCaptureVideoOrientation initialVideoOrientation = AVCaptureVideoOrientationLandscapeRight;
            self.previewView.videoPreviewLayer.connection.videoOrientation = initialVideoOrientation;
        });
    }
    else {
        NSLog(@"Could not add video device input to the session");
        [self.session commitConfiguration];
        return;
    }
    [self.session commitConfiguration];
}

-(IBAction)toggleCamera:(id)sender {
    [self.session removeInput:self.videoDeviceInput];
    
    if ([self.currentlySelectedCamera isEqualToString:AVCaptureDeviceTypeBuiltInUltraWideCamera]) {
        self.currentlySelectedCamera = AVCaptureDeviceTypeBuiltInTelephotoCamera;
    }
    else if ([self.currentlySelectedCamera isEqualToString:AVCaptureDeviceTypeBuiltInTelephotoCamera]) {
        self.currentlySelectedCamera = AVCaptureDeviceTypeBuiltInWideAngleCamera;
    }
    else {
        self.currentlySelectedCamera = AVCaptureDeviceTypeBuiltInUltraWideCamera;
    }
    
    
    AVCaptureDevice* videoDevice = [AVCaptureDevice defaultDeviceWithDeviceType:self.currentlySelectedCamera
                                                                      mediaType:AVMediaTypeVideo
                                                                       position:AVCaptureDevicePositionBack];
    NSError *error;
    AVCaptureDeviceInput* videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error];
    if (!videoDeviceInput) {
        NSLog(@"Could not create video device input: %@", error);
        [self.session commitConfiguration];
        return;
    }
    if ([self.session canAddInput:videoDeviceInput]) {
        [self.session addInput:videoDeviceInput];
        self.videoDeviceInput = videoDeviceInput;
    }

}

-(BOOL)prefersHomeIndicatorAutoHidden {
    return YES;
}

@end
