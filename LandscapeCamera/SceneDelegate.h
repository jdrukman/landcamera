//
//  SceneDelegate.h
//  LandscapeCamera
//
//  Created by Jon Drukman on 4/3/20.
//  Copyright © 2020 Jon Drukman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

